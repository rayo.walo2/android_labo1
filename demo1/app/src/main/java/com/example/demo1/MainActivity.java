package com.example.demo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText username,pw;
    private Button loginBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.username=(EditText)findViewById(R.id.username);
        this.pw=(EditText)findViewById(R.id.pw);
    }
    public void login(View view)
    {
        String userN=this.username.getText().toString();
        String userP=this.pw.getText().toString();

        dbWorker dbw = new dbWorker(this);
        dbw.execute(userN,userP);
    }
    public void signup(View view)
    {
        Intent intent = new Intent(this, signup.class);
        startActivity(intent);
    }
}
