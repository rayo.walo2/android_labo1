package com.example.demo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class activityManip extends AppCompatActivity {
    private TextView nomActView,nbre,note;
    private String user;
    private ListView listView;
    private ArrayAdapter<String> itemsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manip);
        Intent intent = getIntent();
        String nomAct = intent.getExtras().getString("nomActivity");
        user = intent.getExtras().getString("user");
        this.nomActView=findViewById(R.id.nomActView);
        this.nomActView.setText(nomAct);
        activityManip.dbWorker1 dbw = new activityManip.dbWorker1(this);
        dbw.execute(nomAct,user);
    }

    public class dbWorker1 extends AsyncTask
    {
        private Context c;
        public dbWorker1(Context c)
        {
            this.c=c;
        }
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected ArrayList<String> doInBackground(Object[] param)
        {
            String cible ="http://10.1.2.200/AndroidToMySQL/get_nbreInscriptions.php";
            ArrayList<String>l=new ArrayList<>();
            try {
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");


                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                String msg = URLEncoder.encode("nomActivity", "utf-8") + "=" +
                        URLEncoder.encode((String) param[0], "utf-8") ;

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while ((line = bufr.readLine()) != null) {
                    l.add(line);
                }
                return l;
            }
            catch (Throwable t) {
                l.add("not defined");
                l.add("not defined");
            }
            return l;
        }
        @Override
        protected void onPostExecute(Object o)
        {
            final ArrayList<String> l =(ArrayList<String>)o;
            nbre=findViewById(R.id.nbre);
            nbre.setText("Nombre de membres: "+l.get(0));
            note=findViewById(R.id.note);
            note.setText("Note moyenne: "+l.get(1)+"/10");
        }
    }
}
