package com.example.demo1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class dbWorker extends AsyncTask
{
    private Context c;
    private AlertDialog ad;

    public dbWorker(Context c)
    {

        this.c=c;
    }
    @Override
    protected void onPreExecute() {
        this.ad = new AlertDialog.Builder(this.c).create();
        this.ad.setTitle("Login Status");
    }
    @Override
    protected String doInBackground(Object[] param)
    {
        String returnedVal="0";
        String className = c.getClass().getSimpleName();
        switch (className) {
            case "MainActivity":
                if(login(param).equals("1"))
                {
                    return param[0].toString();
                }
            case "signup":
                if(signup(param).equals("1"))
                {
                    return param[0].toString();
                }
        }
        return returnedVal;
    }
    @Override
    protected void onPostExecute(Object o)
    {
        if(o.toString()=="0")
        {
            this.ad.setMessage("wrong user/password");
            this.ad.show();
        }
        else
        {
            Intent i = new Intent(this.c, useCasesActivity.class);
            i.putExtra("user", o.toString());
            c.startActivity(i);
        }
    }
    public String login(Object[] param)
    {
        String cible ="http://10.1.2.200/AndroidToMySQL/login.php";
        try {
            URL url = new URL(cible);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");


            OutputStream outs = con.getOutputStream();
            BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

            String msg = URLEncoder.encode("user", "utf-8") + "=" +
                    URLEncoder.encode((String) param[0], "utf-8") +
                    "&" + URLEncoder.encode("pw", "utf-8") + "=" +
                    URLEncoder.encode((String) param[1], "utf-8");

            bufw.write(msg);
            bufw.flush();
            bufw.close();
            outs.close();

            InputStream ins = con.getInputStream();
            BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
            String line;
            StringBuffer sbuff = new StringBuffer();

            while ((line = bufr.readLine()) != null) {
                sbuff.append(line);
            }
            return sbuff.toString();
        }
        catch (Exception ex)
        {
            return ex.getMessage();
        }
    }
    public String signup(Object[] param)
    {
        String cible ="http://10.1.2.200/AndroidToMySQL/signup.php";
        try {
            URL url = new URL(cible);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setRequestMethod("POST");


            OutputStream outs = con.getOutputStream();
            BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

            String msg = URLEncoder.encode("user", "utf-8") + "=" +
                    URLEncoder.encode((String) param[0], "utf-8") +
                    "&" + URLEncoder.encode("pw", "utf-8") + "=" +
                    URLEncoder.encode((String) param[1], "utf-8")+
                    "&" + URLEncoder.encode("name", "utf-8") + "=" +
                    URLEncoder.encode((String) param[2], "utf-8")+
                    "&" + URLEncoder.encode("lastName", "utf-8") + "=" +
                    URLEncoder.encode((String) param[3], "utf-8")+
                    "&" + URLEncoder.encode("profession", "utf-8") + "=" +
                    URLEncoder.encode((String) param[4], "utf-8");

            bufw.write(msg);
            bufw.flush();
            bufw.close();
            outs.close();

            InputStream ins = con.getInputStream();
            BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
            String line;
            StringBuffer sbuff = new StringBuffer();

            while ((line = bufr.readLine()) != null) {
                sbuff.append(line);
            }
            return sbuff.toString();
        }
        catch (Exception ex)
        {
            return ex.getMessage();
        }
    }
}
