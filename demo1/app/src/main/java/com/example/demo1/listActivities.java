package com.example.demo1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class listActivities extends AppCompatActivity {
    private String user;
    private ListView listView;
    private List<String> activities;
    private ArrayAdapter<String> itemsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_activities);
        Intent intent = getIntent();
        this.user = intent.getExtras().getString("user");
        dbWorker1 dbw = new dbWorker1(this);
        dbw.execute();
    }

    public class dbWorker1 extends AsyncTask
    {
        private Context c;
        public dbWorker1(Context c)
        {
            this.c=c;
        }
        @Override
        protected void onPreExecute() {
            activities = new ArrayList<>();
        }
        @Override
        protected List<String> doInBackground(Object[] param)
        {
            String cible ="http://10.1.2.200/AndroidToMySQL/get_activities.php";
            try {
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");


                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));
                String line;
                StringBuffer sbuff = new StringBuffer();

                while ((line = bufr.readLine()) != null) {
                    activities.add(line.toString());
                }
                return activities;
            }
            catch (Throwable t)
            {
                t.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Object o)
        {
            final ArrayList<String> l =(ArrayList<String>)o;

            listView = findViewById(R.id.listView);
            itemsAdapter = new ArrayAdapter<String>(listActivities.this, android.R.layout.simple_list_item_1,l);
            listView.setAdapter(itemsAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(listActivities.this, activityManip.class);
                    i.putExtra("user", user);
                    i.putExtra("nomActivity", l.get(position));
                    startActivity(i);
                }
            });

        }
    }
}
