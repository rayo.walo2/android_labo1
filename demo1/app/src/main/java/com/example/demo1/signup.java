package com.example.demo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class signup extends AppCompatActivity {
    private EditText username,pw,name,lastname,profession ;
    private Button signupBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        this.username=(EditText)findViewById(R.id.username);
        this.pw=(EditText)findViewById(R.id.pw);
        this.name=(EditText)findViewById(R.id.name);
        this.lastname=(EditText)findViewById(R.id.lastname);
        this.profession =(EditText)findViewById(R.id.profession );
    }
    public void signup(View view)
    {
        String userN=this.username.getText().toString();
        String userP=this.pw.getText().toString();
        String name=this.name.getText().toString();
        String lastName=this.lastname.getText().toString();
        String profession=this.profession.getText().toString();

        dbWorker dbw = new dbWorker(this);
        dbw.execute(userN,userP,name,lastName,profession);
    }
    protected void goBack(View view)
    {
        this.finish();
    }
}
