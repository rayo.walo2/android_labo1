package com.example.demo1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class useCasesActivity extends AppCompatActivity {
    private ListView listView;
    private ArrayAdapter<String> itemsAdapter;
    private String user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_use_cases);
        Intent intent = getIntent();
        this.user = intent.getExtras().getString("user");
        listView = findViewById(R.id.listView);
        final String[] l={"modifier les informations","visualiser la liste des activités","proposer une nouvelle activité"};
        itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,l);
        listView.setAdapter(itemsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:

                    case 1:
                        Intent i = new Intent(useCasesActivity.this, listActivities.class);
                        i.putExtra("user", user);
                        startActivity(i);
                    case 2:

                }

            }
        });
    }
}
